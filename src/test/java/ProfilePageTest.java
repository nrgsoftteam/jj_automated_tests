import configuration.ConfigProperties;
import io.qameta.allure.Step;
import models.User;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.Page;
import pages.ProfilePage;

import java.util.logging.Logger;

public class ProfilePageTest {

    final static Logger logger = Logger.getLogger(System.class.getSimpleName());

    @BeforeClass
    public void login() throws InterruptedException {
        logger.info("Test suit STARTED ");
        Page.getDriver().get(ConfigProperties.getTestProperty("justjoinru"));
        User user = new User(ConfigProperties.getTestProperty("anExistingLogin"), ConfigProperties.getTestProperty("correctPassword"));
        new ProfilePage()
                .login(user);
    }

    @Step("Add, reply and delete comment")
    @Test (groups = {"positive"}, enabled = true)
    public void comment() throws Exception {
       new ProfilePage().comment();
    }

    @Test (groups = ("positive"), enabled = true)
    public void setChangePhoto() throws Exception {
        new ProfilePage().setChangePhoto();
    }

    @Test (groups = ("positive"), enabled = true)
    public void setChangeBackgroundPhoto() throws Exception {
        new ProfilePage().setChangeBackgroundPhoto();
    }

}
