import configuration.ConfigProperties;
import io.qameta.allure.Step;
import models.UserwithoutPass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.ForgotPasswordPage;
import pages.Page;

import java.util.logging.Logger;

public class PasswordTest {

    final static Logger logger = Logger.getLogger(System.class.getSimpleName());

    @BeforeClass
    public void setUp() {
        logger.info("Test suit STARTED ");
        Page.getDriver().get(ConfigProperties.getTestProperty("justjoinru"));
    }
    @Step("Forget Password")
    @Test (groups = {"positive"}, enabled = true)
    public void reset () throws Exception{
        UserwithoutPass uwp = new UserwithoutPass(ConfigProperties.getTestProperty("anExistingLogin"));
        new ForgotPasswordPage()
                .reset(uwp);

    }

    @Step("Un Existing User Forget Password")
    @Test (groups = {"negative"}, enabled = true)

    public void resetunreg() throws Exception {
        UserwithoutPass uwp = new UserwithoutPass(ConfigProperties.getTestProperty("notAnExistingLogin"));
        new ForgotPasswordPage()
                .resetunreg(uwp);
    }
}
