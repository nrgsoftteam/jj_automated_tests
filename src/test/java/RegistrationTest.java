import configuration.ConfigProperties;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import models.UserWithoutEmail;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.Page;
import pages.RegistrationPage;

import java.util.logging.Logger;

import static io.restassured.RestAssured.given;

public class RegistrationTest {

    final static Logger logger = Logger.getLogger(System.class.getSimpleName());

    @BeforeClass
    public void setUp() {
        logger.info("Test suit STARTED ");
        Page.getDriver().get(ConfigProperties.getTestProperty("justjoinru"));
    }

    @Step("Registration")
    @Test(groups = {"positive"}, enabled = true)

    public void register() throws Exception {
        UserWithoutEmail uwe = new UserWithoutEmail(ConfigProperties.getTestProperty("correctPassword"));
        new RegistrationPage()
                .register(uwe);
    }

    @Test
    public void facebookRegistration() throws Exception {
        new RegistrationPage().facebookRegistration();
    }


}


