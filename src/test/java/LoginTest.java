import configuration.ConfigProperties;
import io.qameta.allure.Step;
import models.User;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.Page;

import java.util.logging.Logger;

public class LoginTest {

	final static Logger logger = Logger.getLogger(System.class.getSimpleName());

	@BeforeClass
	public void setUp() {
		logger.info("Test suit STARTED ");
		Page.getDriver().get(ConfigProperties.getTestProperty("justjoinru"));
	}

	@Step ("Login")
	@Test (groups = {"positive"}, enabled = true)
	public void login() throws Exception {
		User user = new User(ConfigProperties.getTestProperty("anExistingLogin"), ConfigProperties.getTestProperty("correctPassword"));
		new LoginPage()
				.login(user);
	}

	@Step ("Invalid Login")
	@Test (groups = {"negative"}, enabled = true)
	public void loginWithWrongEmailAndWrongPassword() throws Exception {
		User user = new User(ConfigProperties.getTestProperty("notAnExistingLogin"), ConfigProperties.getTestProperty("inCorrectPassword"));
		new LoginPage()
				.loginWithWrongEmailAndWrongPassword(user);
	}

	@Test
	public void googleLogin()  {
		new LoginPage().googleLogin();
	}

	@Test
	public void facebookLogin()  {
		new LoginPage().facebookLogin();
	}


}
