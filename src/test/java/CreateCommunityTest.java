import configuration.ConfigProperties;
import models.User;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.*;

import java.util.logging.Logger;

public class CreateCommunityTest extends Page {
    final static Logger logger = Logger.getLogger(System.class.getSimpleName());

    @BeforeSuite
    public void login() throws InterruptedException {
        logger.info("Test suit STARTED ");
        Page.getDriver().get(ConfigProperties.getTestProperty("justjoinru"));
        User user = new User(ConfigProperties.getTestProperty("anExistingLogin"), ConfigProperties.getTestProperty("correctPassword"));
        new ProfilePage()
                .login(user);
    }

    @Test(priority = 1)
    public void simpleCreateCommunity() throws InterruptedException {
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().simpleCommunityCreate();
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().checkCommunitySize();
    }

    @Test(priority = 2)
    public void detailedOpenCommunityCreate() throws InterruptedException {
        new CommunityPage().detailedOpenCommunityCreate();
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().checkCommunitySize();
    }

    @Test(priority = 3)
    public void friendsCommunityCreate() throws InterruptedException {
        new CommunityPage().friendsCommunityCreate();
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().checkCommunitySize();
    }

    @Test(priority = 4)
    public void privateCommunityCreate() throws InterruptedException {
        new CommunityPage().privateCommunityCreate();
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().checkCommunitySize();
    }

    @Test(priority = 5)
    public void createOpenEventFromCommunity() throws InterruptedException {
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().createOpenEventFromCommunity();
        new MainPage().clickEventsMenuButton();
    }

    @Test(priority = 6)
    public void createFriendsEventFromCommunity() throws InterruptedException {
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().createFriendsEventFromCommunity();
    }

    @Test(priority = 7)
    public void createPrivateEventFromCommunity() throws InterruptedException{
        new MainPage().clickCommunityMenuButton();
        new CommunityPage().createPrivateEventFromCommunity();
    }
}
