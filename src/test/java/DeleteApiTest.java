import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class DeleteApiTest {

  public static Response response;
  public static String jsonAsString;

  @BeforeClass
  public void setup(){

  RestAssured.baseURI = "https://api.justjoin1.ru";
  RestAssured.basePath = "/api/users/";
  //RestAssured.basic("alice.kuzmina@nrg-soft.com", "Vtkfvjhb");
  }

  @Test
  public void testDelete(){
    given()
            .header("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5qbEdNRUl5T0RJelEwRTROekF6TVVaR05VVkdRa1pDUkRVeE16QTJSVGs1TmtWRU1EazNNZyJ9.eyJ2ZXJpZmllZCI6dHJ1ZSwiYXBwX21ldGFkYXRhIjp7InJlZnJlc2hfdG9rZW4iOiIxL1JLX2xQak9YR3RiUW9yOURhcWgyU1kwWnp6MVpwX2QzNl82YTlDV3NSS2MifSwiaXNzIjoiaHR0cHM6Ly95bnBsLmF1dGgwLmNvbS8iLCJzdWIiOiJnb29nbGUtb2F1dGgyfDExNTE1MTAzOTYxMDM4OTU4OTk3NCIsImF1ZCI6IkJCTHA2ZFQ5dWcxbXhZNVVJM3h3bGQ2Y0EzVWtuOGFIIiwiaWF0IjoxNTU0MzY1NTc4LCJleHAiOjI1NTQzNjU1NzcsImF0X2hhc2giOiJxaU1ueGM2WUQ4eGx0NnhiekdXT0R3Iiwibm9uY2UiOiJha3c1cklWUjV5dUJvT2Nha1NNN0YtRXE4Qlpyb25rTCJ9.Yd1XFyFsTERLtIM5sH9v4-Q2XCyF0yp1vkgiNK8c7CYjzbr0fyvSsXcaSwY3Gf7JkcPGkAASncJVhAsb6iHz4QuBVAGpGxkk1iQ29djvNbN9-aK-NH8BA5h30JV35UXOft1AD-na9-FLllE-TTKztajsYWJXQIdpAilVexsHV7x48C3xFsH2TrYqy6kOMkp4ImKY1p-fESwBkMGcZBSs-evnoVpmg3DJKTpjArtM0HvIIcaBIcxlGMoMqrudbZVDzBw6v9zU7mr4fKAg6y5Z8SfF6Z-i9074Ai1ZMNbySh8Q4Y2A2kXfKD7KERrJhZ4R5fiztKjCX4PoIrGuT-HpuA")
            .delete("https://app.justjoin1.ru/5c07e1d2297aa646b0d19101/profile")
            .then()
            .statusCode(200);
   }



  //@Test
  //public void testSearch() //{
  //given()

  //.header("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5qbEdNRUl5T0RJelEwRTROekF6TVVaR05VVkdRa1pDUkRVeE16QTJSVGs1TmtWRU1EazNNZyJ9.eyJ2ZXJpZmllZCI6dHJ1ZSwiYXBwX21ldGFkYXRhIjp7InJlZnJlc2hfdG9rZW4iOiIxL1JLX2xQak9YR3RiUW9yOURhcWgyU1kwWnp6MVpwX2QzNl82YTlDV3NSS2MifSwiaXNzIjoiaHR0cHM6Ly95bnBsLmF1dGgwLmNvbS8iLCJzdWIiOiJnb29nbGUtb2F1dGgyfDExNTE1MTAzOTYxMDM4OTU4OTk3NCIsImF1ZCI6IkJCTHA2ZFQ5dWcxbXhZNVVJM3h3bGQ2Y0EzVWtuOGFIIiwiaWF0IjoxNTU0MzY1NTc4LCJleHAiOjI1NTQzNjU1NzcsImF0X2hhc2giOiJxaU1ueGM2WUQ4eGx0NnhiekdXT0R3Iiwibm9uY2UiOiJha3c1cklWUjV5dUJvT2Nha1NNN0YtRXE4Qlpyb25rTCJ9.Yd1XFyFsTERLtIM5sH9v4-Q2XCyF0yp1vkgiNK8c7CYjzbr0fyvSsXcaSwY3Gf7JkcPGkAASncJVhAsb6iHz4QuBVAGpGxkk1iQ29djvNbN9-aK-NH8BA5h30JV35UXOft1AD-na9-FLllE-TTKztajsYWJXQIdpAilVexsHV7x48C3xFsH2TrYqy6kOMkp4ImKY1p-fESwBkMGcZBSs-evnoVpmg3DJKTpjArtM0HvIIcaBIcxlGMoMqrudbZVDzBw6v9zU7mr4fKAg6y5Z8SfF6Z-i9074Ai1ZMNbySh8Q4Y2A2kXfKD7KERrJhZ4R5fiztKjCX4PoIrGuT-HpuA")
  //.get("https://api.justjoin1.ru/api/admin/users")
  //.then()
  //.body("[95].email", Matchers.equalTo("daniel_japywmo_sharpesen@tfbnw.net"));

  //}
  }

