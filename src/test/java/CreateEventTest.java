import configuration.ConfigProperties;
import models.User;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import pages.EventPage;
import pages.MainPage;
import pages.Page;
import pages.ProfilePage;

import java.util.logging.Logger;

public class CreateEventTest extends Page {

  final static Logger logger = Logger.getLogger(System.class.getSimpleName());

  @BeforeSuite
  public void login() throws InterruptedException {
    logger.info("Test suit STARTED ");
    Page.getDriver().get(ConfigProperties.getTestProperty("justjoinru"));
    User user = new User(ConfigProperties.getTestProperty("anExistingLogin"), ConfigProperties.getTestProperty("correctPassword"));
    new ProfilePage()
            .login(user);
  }

  @Test(priority = 1)
  public void createSimpleEvent() throws Exception {
    new MainPage().clickEventsMenuButton();
    new EventPage().createSimpleEvent();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();

  }

  @Test(priority = 2)
  public void createMoreOpenEvent() throws InterruptedException {
    new EventPage().createMoreOpenEvent();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 3)
  public void createMoreFriendsEvent() throws InterruptedException {
    new EventPage().createMoreFriendsEvent();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 4)
  public void createMorePrivateEvent() throws InterruptedException {
    new EventPage().createMorePrivateEvent();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 5)
  public void createRepeatedEvent() throws InterruptedException {
    new EventPage().createRepeatedEvent();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 6)
  public void createRepeatedFriendEvent() throws InterruptedException {
    new EventPage().createRepeatedFriendEvent();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 7)
  public void createRepeatedPrivateEvent() throws InterruptedException {
    new EventPage().createRepeatedPrivateEvent();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 8)
  public void createRepeatedEventOneMonthDurationThreeDays() throws InterruptedException {
    new EventPage().createRepeatedEventOneMonthDurationThreeDays();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 9)
  public void createRepeatedEventThreeMonthsDurationThreeDays() throws InterruptedException {
    new EventPage().createRepeatedEventThreeMonthsDurationThreeDays();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 10)
  public void createRepeatedEventSixMonthsDurationThreeDays() throws InterruptedException {
    new EventPage().createRepeatedEventSixMonthsDurationThreeDays();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 11)
  public void createRepeatedTwelveMonthDurationThreeDays() throws InterruptedException {
    new EventPage().createRepeatedTwelveMonthDurationThreeDays();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

  @Test(priority = 12)
  public void createRepeatedTwelveMonthDurationTwoDays() throws InterruptedException {
    new EventPage().createRepeatedTwelveMonthDurationTwoDays();
    new MainPage().clickEventsMenuButton();
    new EventPage().checkEventSize();
  }

}

