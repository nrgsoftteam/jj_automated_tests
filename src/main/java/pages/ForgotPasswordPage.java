package pages;

import models.UserwithoutPass;
import org.apache.bcel.ExceptionConstants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class ForgotPasswordPage extends Page{
    @FindBy(xpath = "//button[@id='goToJoin']")
    WebElement buttonGoJoin;

    @FindBy (xpath = "//div[@id='mat-tab-label-0-0']")
    WebElement loginLink;

    @FindBy (xpath = "//mat-tab-body[@id='mat-tab-content-0-0']/div/section/div[3]/span")
    WebElement emailLink;

    @FindBy (xpath = "//input[@id='mat-input-0']")
    WebElement email;

    @FindBy (xpath = "//mat-tab-body[@id='mat-tab-content-0-0']/div/section/div[3]/div/div/span")
    WebElement forgetButton;

    @FindBy (xpath = "//button[@class='jj-button mat-raised-button mat-primary']")
    WebElement changeButton;

    @FindBy (xpath = "//button[@class='jj-button mat-button mat-primary']")
    WebElement backButton;

    @FindBy(css = ".rp-success")
    WebElement removePasswordEmailMessage;

    final String removePasswordEmailMessageIsSend = "Мы отправили сообщение на почту alice.kuzmina+1102@nrg-soft.com";

    public ForgotPasswordPage reset(UserwithoutPass userwithoutPass) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        buttonGoJoin.click();

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='mat-tab-label-0-0']"))).click();

        emailLink.click();

        email.click();
        email.sendKeys(userwithoutPass.getLogin());

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//mat-tab-body[@id='mat-tab-content-0-0']/div/section/div[3]/div/div/span"))).click();

        changeButton.click();
        removePasswordEmailMessage.click();
        String textMessageAboutResendPassword = removePasswordEmailMessage.getText();
        Assert.assertTrue(textMessageAboutResendPassword.equals(removePasswordEmailMessageIsSend));
        backButton.click();

        return new ForgotPasswordPage();
    }

    public ForgotPasswordPage resetunreg(UserwithoutPass userwithoutPass) throws Exception {
        new WebDriverWait(driver,20);
        buttonGoJoin.click();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        loginLink.click();

        emailLink.click();

        email.click();
        email.sendKeys(userwithoutPass.getLogin());

        Thread.sleep(3000);
        forgetButton.click();
        Thread.sleep(3000);
        changeButton.click();
        Thread.sleep(3000);
        backButton.click();

        return new ForgotPasswordPage();
    }
}
