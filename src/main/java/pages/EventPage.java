package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class EventPage extends Page {
  @FindBy(xpath = "//button[@ng-click='$ctrl.editEventPage()']")
  WebElement createEventButton;

  @FindBy(xpath = "//input[@name='activity']")
  WebElement getSelectEventActivityField;

  /*@FindBy(xpath = "//button[@ng-click='$ctrl.formSwitch()']")
  WebElement moreOptionButton; */

  @FindBy(xpath = "//input[@name='title']")
  WebElement eventNameField;

  @FindBy(xpath = "//textarea[@ng-model='$ctrl.event.comments']")
  WebElement eventDescriptionField;

  @FindBy(xpath = "//input[@name='facebook']")
  WebElement facebookLink;

  @FindBy(xpath = "//md-select[@ng-model='$ctrl.event.visibility']")
  WebElement selectType;

  /*@FindBy(xpath = "//md-option[@value='friends']")
  WebElement selectFriendsType;*/

  /*@FindBy(xpath = "//md-option[@value='private']")
  WebElement selectPrivateType;*/

  /*@FindBy(css = ".md-container")
  WebElement repeatedEventCheckbox;*/

  @FindBy(xpath = "//span[contains(.,'sat')]")
  WebElement selectDayOne;

  @FindBy(xpath = "//span[contains(.,'sun')]")
  WebElement selectDayTwo;

  @FindBy(css = ".selected-items:nth-child(4) > .select-month > .ng-binding")
  WebElement select12Months;

  @FindBy(css = ".selected-items:nth-child(2) > .select-month > .ng-binding")
  WebElement select3Months;

  @FindBy(css = ".selected-items:nth-child(3) > .select-month > .ng-binding")
  WebElement select6Months;

  @FindBy(xpath = "//button[@type='submit']")
  WebElement saveEventButton;

  /*@FindBy(xpath = "//div[@class='container-every-days layout-row']")
  WebElement everyDaysText;*/

  @FindBy(css = ".event-background")
  WebElement eventBackground;

  @FindBy(xpath = "//button[@ng-click='$ctrl.editEvent(event)']")
  WebElement editEventButton;

  @FindBy(xpath = "//div[2]/div/div/span")
  WebElement editingRepeatedEventname;

  @FindBy(xpath = "//div[contains(@class, 'facebook')]")
  WebElement facebookIcon;

  @FindBys(@FindBy(xpath = "//div[@class='md-body-1 jj-card layout-column flex']//div[contains(@ng-repeat,'events')]"))
  List<WebElement> sizeOfEvents;

  int newSizeOfEvents;

  private void selectOptionWithTextA(String athletics) {
    try {
      WebElement autoOptions = driver.findElement(By.className("md-autocomplete-suggestions"));

      List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
      for (WebElement option : optionsToSelect) {
        if (option.getText().equals(athletics)) {
          System.out.println("Trying to select: " + athletics);
          option.click();
          break;
        }
      }

    } catch ( NoSuchElementException e ) {
      System.out.println(e.getStackTrace());
    } catch ( Exception e ) {
      System.out.println(e.getStackTrace());
    }
  }

  private void selectOptionWithTextB(String badminton) {
    try {
      WebElement autoOptions = driver.findElement(By.className("md-autocomplete-suggestions"));

      List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
      for (WebElement option : optionsToSelect) {
        if (option.getText().equals(badminton)) {
          System.out.println("Trying to select: " + badminton);
          option.click();
          break;
        }
      }

    } catch ( NoSuchElementException e ) {
      System.out.println(e.getStackTrace());
    } catch ( Exception e ) {
      System.out.println(e.getStackTrace());
    }
  }

  private void selectOptionWithTextC(String Card) {
    try {
      WebElement autoOptions = driver.findElement(By.className("md-autocomplete-suggestions"));

      List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
      for (WebElement option : optionsToSelect) {
        if (option.getText().equals(Card)) {
          System.out.println("Trying to select: " + Card);
          option.click();
          break;
        }
      }

    } catch ( NoSuchElementException e ) {
      System.out.println(e.getStackTrace());
    } catch ( Exception e ) {
      System.out.println(e.getStackTrace());
    }
  }

  public void checkEventBackground() {
    Assert.assertTrue(eventBackground.isDisplayed());
  }

  public void checkEventBackgroundAndFacebookIcon() {
    Assert.assertTrue(eventBackground.isDisplayed());
    Assert.assertTrue(facebookIcon.isDisplayed());
  }

  public int getEventSize() {
    return newSizeOfEvents = sizeOfEvents.size();
  }


  public void checkEventSize() {
    Assert.assertNotEquals(getEventSize(), newSizeOfEvents + 1);
  }

  public EventPage createSimpleEvent() throws Exception {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ath");
    selectOptionWithTextA("Athletics");

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='submit']")));
    saveEventButton.click();

    checkEventBackground();
    Thread.sleep(3000);

    return new EventPage();
  }


  public EventPage createMoreOpenEvent() throws InterruptedException {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("bad");
    selectOptionWithTextB("Badminton");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='$ctrl.formSwitch()']"))).click();
    eventNameField.sendKeys("Badminton big game for everyone");
    eventDescriptionField.sendKeys("Celebrated delightful an especially increasing instrument am. Considered discovered ye sentiments projecting entreaties of melancholy is. Draw fond rank form nor the day eat. Way own uncommonly travelling now acceptance bed compliment solicitude. Am wound worth water he linen at vexed.");
    facebookLink.sendKeys("https://www.facebook.com/");
    saveEventButton.click();

    Thread.sleep(3000);
    checkEventBackgroundAndFacebookIcon();

    return new EventPage();
  }

  public EventPage createMoreFriendsEvent() throws InterruptedException {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("bad");
    selectOptionWithTextB("Badminton");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='$ctrl.formSwitch()']"))).click();
    eventNameField.sendKeys("Badminton big game for friends");
    eventDescriptionField.sendKeys("Celebrated delightful an especially increasing instrument am. Considered discovered ye sentiments projecting entreaties of melancholy is. Draw fond rank form nor the day eat. Way own uncommonly travelling now acceptance bed compliment solicitude. Am wound worth water he linen at vexed.");
    facebookLink.sendKeys("https://www.facebook.com/");

    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].scrollIntoView();", selectType);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-select[@ng-model='$ctrl.event.visibility']"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-option[@value='friends']"))).click();

    saveEventButton.click();

    Thread.sleep(3000);
    checkEventBackgroundAndFacebookIcon();
    return new EventPage();
  }

  public EventPage createMorePrivateEvent() throws InterruptedException {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("bad");
    selectOptionWithTextB("Badminton");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='$ctrl.formSwitch()']"))).click();
    eventNameField.sendKeys("Badminton big game private event");
    eventDescriptionField.sendKeys("Celebrated delightful an especially increasing instrument am. Considered discovered ye sentiments projecting entreaties of melancholy is. Draw fond rank form nor the day eat. Way own uncommonly travelling now acceptance bed compliment solicitude. Am wound worth water he linen at vexed.");
    facebookLink.sendKeys("https://www.facebook.com/");

    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].scrollIntoView();", selectType);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-select[@ng-model='$ctrl.event.visibility']"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-option[@value='private']"))).click();

    saveEventButton.click();

    Thread.sleep(3000);
    checkEventBackgroundAndFacebookIcon();
    return new EventPage();
  }

  public EventPage createRepeatedEvent() throws InterruptedException {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("bad");
    selectOptionWithTextB("Badminton");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='$ctrl.formSwitch()']"))).click();
    eventNameField.sendKeys("Badminton Repeated Events for everyone");
    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();
    selectDayOne.click();
    saveEventButton.click();

    return new EventPage();
  }

  public EventPage createRepeatedFriendEvent() throws InterruptedException {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ca");
    selectOptionWithTextC("Card games");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='$ctrl.formSwitch()']"))).click();
    eventNameField.sendKeys("Card games Repeated Events For Friends");

    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();
    selectDayOne.click();
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].scrollIntoView();", selectType);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-select[@ng-model='$ctrl.event.visibility']"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-option[@value='friends']"))).click();

    saveEventButton.click();

    return new EventPage();
  }

  public EventPage createRepeatedPrivateEvent() throws InterruptedException {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ca");
    selectOptionWithTextC("Card games");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@ng-click='$ctrl.formSwitch()']"))).click();
    eventNameField.sendKeys("Card games Repeated Private Events");

    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();
    selectDayOne.click();

    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("arguments[0].scrollIntoView();", selectType);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-select[@ng-model='$ctrl.event.visibility']"))).click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-option[@value='private']"))).click();

    saveEventButton.click();

    return new EventPage();
  }


  public EventPage createRepeatedEventOneMonthDurationThreeDays() {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ca");
    selectOptionWithTextC("Card games");


    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();
    selectDayOne.click();
    selectDayTwo.click();

    saveEventButton.click();

    return new EventPage();
  }

  public EventPage createRepeatedEventThreeMonthsDurationThreeDays() {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ca");
    selectOptionWithTextC("Card games");


    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();
    selectDayOne.click();
    selectDayTwo.click();

    select3Months.click();

    saveEventButton.click();

    return new EventPage();
  }

  public EventPage createRepeatedEventSixMonthsDurationThreeDays() {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ba");
    selectOptionWithTextB("Badminton");


    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();
    selectDayOne.click();
    selectDayTwo.click();

    select6Months.click();

    saveEventButton.click();

    return new EventPage();
  }

  public EventPage createRepeatedTwelveMonthDurationTwoDays() {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ca");
    selectOptionWithTextC("Card games");


    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();
    selectDayOne.click();

    select12Months.click();

    saveEventButton.click();

    return new EventPage();

  }

  public EventPage createRepeatedTwelveMonthDurationThreeDays() {
    getEventSize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.editEventPage()']")));
    createEventButton.click();

    getSelectEventActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ca");
    selectOptionWithTextC("Card games");


    wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".md-container"))).click();

    selectDayOne.click();
    selectDayTwo.click();

    select12Months.click();

    saveEventButton.click();

    return new EventPage();
  }

  /*
  public EventPage editEvent() throws InterruptedException {

    editEventButton.click();

    Thread.sleep(2000);
    moreOptionButton.click();

    eventNameField.sendKeys("Edited event with long name for neighbours");

    eventDescriptionField.sendKeys("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.");
    facebookLink.sendKeys("https://www.facebook.com/");

    Thread.sleep(2000);
    saveEventButton.click();

    return new EventPage();
  }
 */
}
