package pages;

import models.UserWithoutEmail;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RegistrationPage extends Page {

    @FindBy(xpath = "//button[@id='goToJoin']")
    WebElement buttonGoJoin;

    @FindBy(xpath = "//span[@class='accept-text']")
    WebElement acceptText;

    @FindBy(xpath = "//button[@class='jj-button mat-raised-button mat-primary']")
    WebElement acceptButton;

    @FindBy (css = ".a-facebook-button span")
    WebElement facebookButton;

    @FindBy (xpath = "//input[@id='email']")
    WebElement facebookEmailField;

    @FindBy (xpath = "//input[@id='pass']")
    WebElement facebookPasswordField;

    @FindBy (xpath = "//button[@id='loginbutton']")
    WebElement facebookLoginButton;

    @FindBy (xpath = "//button[@name='__CONFIRM__']")
    WebElement facebookConfirmButton;

    @FindBy(xpath = "//span[@class='a-custom-button cursor-pointer ng-star-inserted']")
    WebElement registrationEmailLink;

    @FindBy(xpath = "//input[@id='mat-input-2']")
    WebElement email;

    @FindBy(xpath = "//input[@id='mat-input-3']")
    WebElement password;

    @FindBy(xpath = "//button[@class='jj-button mat-raised-button mat-primary']")
    WebElement registrationButton;

    @FindBy(xpath = "//input[@id='input_8']")
    WebElement nameField;

    @FindBy(xpath = "//input[@id='input_9']")
    WebElement surnameField;

    @FindBy(css = "#select_14")
    WebElement selectYField;

    @FindBy(css = "#select_option_72")
    WebElement selectYear;

    @FindBy(css = "#select_12")
    WebElement selectMField;

    @FindBy(css = "#select_option_58")
    WebElement selectMonth;

    @FindBy(css = "#select_10")
    WebElement selectDField;

    @FindBy(css = "#select_option_29")
    WebElement selectDate;

    @FindBy(css = "#select_18")
    WebElement selectGField;

    @FindBy(css = "#select_option_17")
    WebElement selectGender;

    @FindBy(xpath = "//input[@id='input-160']")
    WebElement languageField;

    @FindBy(xpath = "(.//*[normalize-space(text()) and normalize-space(.)='upload'])[1]/following::span[1]")
    WebElement languageEnter;

    @FindBy(xpath = "//button[@class='md-raised md-primary jj-button jj-button-adaptive md-button md-ink-ripple']")
    WebElement nextButton;

    @FindBy(xpath = "//input[@id='input-22']")
    WebElement searchActivityField;

    @FindBy(xpath = "(.//*[normalize-space(text()) and normalize-space(.)='upload'])[1]/following::span[1]")
    WebElement selectActivity;

    @FindBy(xpath = "(.//*[normalize-space(text()) and normalize-space(.)='Баскетбол'])[1]/following::div[3]")
    WebElement selectSecondActivity;

    @FindBy(xpath = "(.//*[normalize-space(text()) and normalize-space(.)='Шашки'])[1]/following::button[1]")
    WebElement slaiderButton;

    @FindBy(xpath = "(.//*[normalize-space(text()) and normalize-space(.)='Водное поло'])[1]/following::span[1]")
    WebElement selectThirdActivity;

    @FindBy(css = ".formActivityWizard .md-raised")
    WebElement nextButtonTwo;

    @FindBy(css = ".md-raised:nth-child(2)")
    WebElement nextButtonThree;

    public RegistrationPage register(UserWithoutEmail userWithoutEmail) throws Exception {
        new WebDriverWait(driver, 20);
        DateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
        Date today = Calendar.getInstance().getTime();
        String todayDate = df.format(today);
        String newmail = "alice.kuzmina+" + todayDate + "@nrg-soft.com";

        buttonGoJoin.click();

        Thread.sleep(2000);
        acceptText.getAttribute("Чтобы зарегистрироваться, вам следует принять Пользовательское соглашение");

        acceptButton.click();

        registrationEmailLink.click();

        email.click();
        email.sendKeys(newmail);

        password.click();
        password.sendKeys(userWithoutEmail.getPassword());

        registrationButton.click();
        Thread.sleep(3000);

        Thread.sleep(2000);
        nameField.click();
        nameField.sendKeys("Alice");

        Thread.sleep(2000);
        surnameField.click();
        surnameField.sendKeys("Kuzmina");

        Thread.sleep(2000);
        selectYField.click();
        Thread.sleep(2000);
        selectYear.click();

        Thread.sleep(2000);
        selectMField.click();
        Thread.sleep(2000);
        selectMonth.click();

        Thread.sleep(1000);
        selectDField.click();
        Thread.sleep(1000);
        selectDate.click();

        Thread.sleep(1000);
        selectGField.click();
        Thread.sleep(1000);
        selectGender.click();

        languageField.click();
        Thread.sleep(3000);
        languageField.sendKeys("Chinese");
        Thread.sleep(3000);
        languageEnter.click();

        nextButton.click();

        Thread.sleep(3000);
        searchActivityField.click();
        Thread.sleep(3000);
        searchActivityField.sendKeys("Snowbo");
        Thread.sleep(2000);
        selectActivity.click();

        Thread.sleep(2000);
        selectSecondActivity.click();
        Thread.sleep(2000);
        slaiderButton.click();
        Thread.sleep(2000);
        selectThirdActivity.click();

        Thread.sleep(2000);
        nextButtonTwo.click();

        nextButtonThree.click();

        return new RegistrationPage();

    }

    public void facebookRegistration() throws Exception {
        buttonGoJoin.click();

        Thread.sleep(2000);
        acceptText.getAttribute("Чтобы зарегистрироваться, вам следует принять Пользовательское соглашение");

        acceptButton.click();
        facebookButton.click();
        facebookEmailField.click();
        facebookEmailField.sendKeys("daniel_japywmo_sharpesen@tfbnw.net");
        facebookPasswordField.click();
        facebookPasswordField.sendKeys("1234567890!");
        facebookLoginButton.click();
        //facebookConfirmButton.click();

        Thread.sleep(2000);
        selectYField.click();
        Thread.sleep(2000);
        selectYear.click();

        Thread.sleep(2000);
        selectMField.click();
        Thread.sleep(2000);
        selectMonth.click();

        Thread.sleep(1000);
        selectDField.click();
        Thread.sleep(1000);
        selectDate.click();

        Thread.sleep(1000);
        selectGField.click();
        Thread.sleep(1000);
        selectGender.click();

        languageField.click();
        Thread.sleep(3000);
        languageField.sendKeys("Chinese");
        Thread.sleep(3000);
        languageEnter.click();

        nextButton.click();

        Thread.sleep(3000);
        searchActivityField.click();
        Thread.sleep(3000);
        searchActivityField.sendKeys("Snowbo");
        Thread.sleep(2000);
        selectActivity.click();

        Thread.sleep(2000);
        selectSecondActivity.click();
        Thread.sleep(2000);
        slaiderButton.click();
        Thread.sleep(2000);
        selectThirdActivity.click();

        Thread.sleep(2000);
        nextButtonTwo.click();

        nextButtonThree.click();

    }
}
