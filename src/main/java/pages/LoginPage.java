package pages;

import models.User;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class LoginPage extends Page {

  @FindBy(xpath = "//button[@id='goToJoin']")
  WebElement buttonGoJoin;

  @FindBy(xpath = "//div[@id='mat-tab-label-0-0']")
  WebElement loginLink;

  @FindBy(xpath = "//mat-tab-body[@id='mat-tab-content-0-0']/div/section/div[3]/span")
  WebElement emailLink;

  @FindBy(xpath = "//input[@id='mat-input-0']")
  WebElement email;

  @FindBy(xpath = "//input[@id='mat-input-1']")
  WebElement password;

  @FindBy(xpath = "//button[@class='jj-button mat-raised-button mat-primary']")
  WebElement loginButton;

  @FindBy(xpath = "//span[@class='ng-star-inserted']")
  WebElement loginFormTextWrongEmail;

  @FindBy(css = ".a-google-button span")
  WebElement googleButton;

  @FindBy(xpath = "//input[@id='identifierId']")
  WebElement googleEmailField;

  @FindBy(css = "#identifierNext .RveJvd")
  WebElement googleNextButton;

  @FindBy(xpath = "//input[@type='password']")
  WebElement googlePasswordField;

  @FindBy(css = "#passwordNext .RveJvd")
  WebElement googleNextButton2;

  @FindBy(css = ".a-facebook-button span")
  WebElement facebookButton;

  @FindBy(xpath = "//input[@id='email']")
  WebElement facebookEmailField;

  @FindBy(xpath = "//input[@id='pass']")
  WebElement facebookPasswordField;

  @FindBy(xpath = "//button[@id='loginbutton']")
  WebElement facebookLoginButton;

  final String textMissingEmail = "Wrong email or password.";

  public LoginPage login(User user) throws Exception {
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@id='goToJoin']")));
    buttonGoJoin.click();

    Thread.sleep(2000);
    loginLink.click();

    emailLink.click();

    email.click();
    email.sendKeys(user.getLogin());

    password.click();
    password.sendKeys(user.getPassword());

    loginButton.click();

    return new LoginPage();

  }

  public LoginPage loginWithWrongEmailAndWrongPassword(User user)  {
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@id='goToJoin']")));
    buttonGoJoin.click();

    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='mat-tab-label-0-0']")));
    loginLink.click();

    emailLink.click();

    email.click();
    email.sendKeys(user.getLogin());

    password.click();
    password.sendKeys(user.getPassword());

    loginButton.click();
    loginFormTextWrongEmail.click();
    String textError = loginFormTextWrongEmail.getText();
    Assert.assertTrue(textError.equals(textMissingEmail));

    return this;

  }

  public void googleLogin()  {
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@id='goToJoin']")));

    buttonGoJoin.click();

    loginLink.click();

    googleButton.click();
    googleEmailField.click();
    googleEmailField.sendKeys("alice.kuzmina@nrg-soft.com");
    googleNextButton.click();

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='password']")));
    googlePasswordField.click();

    googlePasswordField.sendKeys("Vtkfvjhb");
    googleNextButton2.click();

  }

  public void facebookLogin()  {
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@id='goToJoin']")));
    buttonGoJoin.click();

    loginLink.click();
    facebookButton.click();
    facebookEmailField.click();
    facebookEmailField.sendKeys("jennifer_suafxrm_sharpeberg@tfbnw.net");
    facebookPasswordField.click();
    facebookPasswordField.sendKeys("1234567890!");
    facebookLoginButton.click();
  }


}
