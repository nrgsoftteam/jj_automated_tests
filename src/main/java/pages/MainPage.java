package pages;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage extends Page {

	@FindBy(css = ".sidenav-item:nth-child(2) > .md-title")
	WebElement eventsMenuButton;

  @FindBy(css = ".sidenav-item:nth-child(3) > .md-title")
  WebElement communityMenuButton;
	public void clickEventsMenuButton() throws InterruptedException {
		Thread.sleep(5000);
		eventsMenuButton.click();
	}

	public void clickCommunityMenuButton() throws InterruptedException {
    Thread.sleep(5000);
    communityMenuButton.click();

  }

	public static byte[] takeScreenshot() {
		return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
	}

}
