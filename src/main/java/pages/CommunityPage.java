package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class CommunityPage extends Page {

  /*@FindBy(xpath = "//button[@ng-click='$ctrl.createCommunityPage()']")
  WebElement createCommunityButton;*/

  @FindBy(xpath = "//input[@name='activity']")
  WebElement getSelectCommunityActivityField;

  @FindBy(xpath = "//input[@name='title']")
  WebElement communityNameField;

  @FindBy(xpath = "//textarea[@ng-model='$ctrl.community.description']")
  WebElement communityDescriptionField;

  @FindBy(xpath = "//input[@ng-model='$ctrl.community.companyName']")
  WebElement communityCompanyNameField;

  @FindBy(xpath = "//input[@name='facebook']")
  WebElement facebookLink;

  @FindBy(xpath = "//input[@name='twitter']")
  WebElement twitterLink;

  /*@FindBy(xpath = "//md-select[@ng-model='$ctrl.community.visibility']")
  WebElement selectType;*/

  /*@FindBy(xpath = "//md-option[@value='friends']")
  WebElement selectFriendsType;*/

  /*@FindBy(xpath = "//md-option[@value='private']")
  WebElement selectPrivateType;*/

  @FindBy(xpath = "//button[@type='submit']")
  WebElement saveCommunityButton;

  /*@FindBy(xpath = "//button[@ng-click='$ctrl.followCommunity($ctrl.data.community)']")
  WebElement joinCommunityButton;*/

  @FindBy(xpath = "//span[contains(.,'Open Backgammon community for neighbourhoods')]")
  WebElement openCommunity;

  @FindBy(css = ".event-actions > .jj-button-absolute > .jj-button-text")
  WebElement createEventViaCommunityButton;

  @FindBy(xpath = "//button[@ng-click='$ctrl.formSwitch()']")
  WebElement moreOptionButton;

  @FindBy(xpath = "//md-select[@ng-model='$ctrl.event.visibility']")
  WebElement selectEventType;

  @FindBy(xpath = "//md-option[@value='friends']")
  WebElement selectEventFriendsType;

  @FindBy(xpath = "//md-option[@value='private']")
  WebElement selectEventPrivateType;

  @FindBy(xpath = "//button[@type='submit']")
  WebElement saveEventButton;

  @FindBys(@FindBy(xpath = "//div[@class='md-body-1 jj-card layout-column flex']//div[contains(@ng-repeat,'community')]"))
  List<WebElement> sizeOfCommunity;
  int newSizeOfCommunity;

  public int getCommunitySize() {
    return newSizeOfCommunity = sizeOfCommunity.size();
  }

  public void checkCommunitySize() {
    Assert.assertNotEquals(getCommunitySize(), newSizeOfCommunity + 1);
  }

  private void selectOptionWithTextA(String archery) {
    try {
      WebElement autoOptions = driver.findElement(By.className("md-autocomplete-suggestions"));

      List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
      for (WebElement option : optionsToSelect) {
        if (option.getText().equals(archery)) {
          option.click();
          break;
        }
      }

    } catch ( NoSuchElementException e ) {
      System.out.println(e.getStackTrace());
    } catch ( Exception e ) {
      System.out.println(e.getStackTrace());
    }
  }

  private void selectOptionWithTextB(String backgammon) {
    try {
      WebElement autoOptions = driver.findElement(By.className("md-autocomplete-suggestions"));

      List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
      for (WebElement option : optionsToSelect) {
        if (option.getText().equals(backgammon)) {
          System.out.println("Trying to select: " + backgammon);
          option.click();
          break;
        }
      }

    } catch ( NoSuchElementException e ) {
      System.out.println(e.getStackTrace());
    } catch ( Exception e ) {
      System.out.println(e.getStackTrace());
    }
  }

  private void selectOptionWithTextC(String cricket) {
    try {
      WebElement autoOptions = driver.findElement(By.className("md-autocomplete-suggestions"));

      List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
      for (WebElement option : optionsToSelect) {
        if (option.getText().equals(cricket)) {
          System.out.println("Trying to select: " + cricket);
          option.click();
          break;
        }
      }

    } catch ( NoSuchElementException e ) {
      System.out.println(e.getStackTrace());
    } catch ( Exception e ) {
      System.out.println(e.getStackTrace());
    }
  }

  private void selectOptionWithTextD(String darts) {
    try {
      WebElement autoOptions = driver.findElement(By.className("md-autocomplete-suggestions"));

      List<WebElement> optionsToSelect = autoOptions.findElements(By.tagName("li"));
      for (WebElement option : optionsToSelect) {
        if (option.getText().equals(darts)) {
          System.out.println("Trying to select: " + darts);
          option.click();
          break;
        }
      }

    } catch ( NoSuchElementException e ) {
      System.out.println(e.getStackTrace());
    } catch ( Exception e ) {
      System.out.println(e.getStackTrace());
    }
  }


  public CommunityPage simpleCommunityCreate() {
    getCommunitySize();

    WebDriverWait wait = new WebDriverWait(driver, 50);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.createCommunityPage()']"))).click();

    getSelectCommunityActivityField.click();
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='activity']"))).sendKeys("arc");
    selectOptionWithTextA("Archery");

    communityNameField.sendKeys("Open Archery community for neighbourhoods");
    saveCommunityButton.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.followCommunity($ctrl.data.community)']"))).click();

    return new CommunityPage();
  }

  public CommunityPage detailedOpenCommunityCreate() {
    getCommunitySize();

    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.createCommunityPage()']"))).click();

    getSelectCommunityActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("ba");
    selectOptionWithTextB("Backgammon");

    communityNameField.sendKeys("Open Backgammon community for neighbourhoods");
    communityDescriptionField.sendKeys("On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.");
    communityCompanyNameField.sendKeys("Open Backgammon KR company");
    facebookLink.sendKeys("https://www.facebook.com/");
    saveCommunityButton.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.followCommunity($ctrl.data.community)']"))).click();

    return new CommunityPage();
  }

  public CommunityPage friendsCommunityCreate() {
    getCommunitySize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.createCommunityPage()']"))).click();

    getSelectCommunityActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("cr");
    selectOptionWithTextC("Cricket");

    communityNameField.sendKeys("Cricket Friends community for neighbourhoods");
    communityDescriptionField.sendKeys("On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.");
    communityCompanyNameField.sendKeys("Cricket company");
    facebookLink.sendKeys("https://www.facebook.com/");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-select[@ng-model='$ctrl.community.visibility']"))).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//md-option[@value='friends']"))).click();

    saveCommunityButton.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.followCommunity($ctrl.data.community)']"))).click();

    return new CommunityPage();
  }

  public CommunityPage privateCommunityCreate() {
    getCommunitySize();
    WebDriverWait wait = new WebDriverWait(driver, 20);
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.createCommunityPage()']"))).click();

    getSelectCommunityActivityField.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='activity']"))).sendKeys("da");
    selectOptionWithTextD("Darts");

    communityNameField.sendKeys("Darts private community");
    communityDescriptionField.sendKeys("On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain.");
    communityCompanyNameField.sendKeys("Darts dot com company");
    facebookLink.sendKeys("https://www.facebook.com/");
    twitterLink.sendKeys("https://twitter.com");

    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//md-select[@ng-model='$ctrl.community.visibility']"))).click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//md-option[@value='private']"))).click();

    saveCommunityButton.click();
    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@ng-click='$ctrl.followCommunity($ctrl.data.community)']"))).click();

    return new CommunityPage();
  }

  public void createOpenEventFromCommunity() throws InterruptedException {

    Thread.sleep(5000);
    openCommunity.click();

    createEventViaCommunityButton.click();
    Thread.sleep(5000);

    saveEventButton.click();
  }

  public void createFriendsEventFromCommunity() throws InterruptedException {
    Thread.sleep(5000);
    openCommunity.click();

    createEventViaCommunityButton.click();

    Thread.sleep(5000);
    moreOptionButton.click();
    selectEventType.click();
    Thread.sleep(3000);
    selectEventFriendsType.click();

    saveEventButton.click();
  }

  public void createPrivateEventFromCommunity() throws InterruptedException {

    Thread.sleep(5000);
    openCommunity.click();

    createEventViaCommunityButton.click();

    Thread.sleep(5000);
    moreOptionButton.click();
    selectEventType.click();
    Thread.sleep(3000);
    selectEventPrivateType.click();

    saveEventButton.click();

  }

}