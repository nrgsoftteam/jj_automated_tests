package pages;

import configuration.ConfigProperties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

public abstract class Page {

    public static WebDriver driver;

    public Page() {

        PageFactory.initElements(getDriver(), this);

    }

    public static WebDriver getDriver() {
        if (driver == null) {

            ChromeOptions options = new ChromeOptions();
            options.addArguments("start-maximized"); // open Browser in maximized mode
            options.addArguments("disable-infobars"); // disabling infobars
            options.addArguments("--disable-extensions"); // disabling extensions
            options.addArguments("--disable-notifications");
            options.addArguments("--disable-gpu"); // applicable to windows os only
            options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
            options.addArguments("--no-sandbox"); // Bypass OS security model

            System.setProperty("webdriver.chrome.driver", ConfigProperties.getTestProperty("chrome"));
            //System.setProperty("webdriver.chrome.driver", "src/test/resources/webDriver/chromedriver_linux64/chromedriver");
            driver = new ChromeDriver(options);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        }


        return driver;

    }

    public static boolean elementExists(String xpath) {
        return getDriver().findElements(By.xpath(xpath)).isEmpty();
    }

    public void addAttachment(WebElement place, String path) {
        File file = new File(path);
        place.sendKeys(file.getAbsolutePath());
    }


}
