package pages;

import models.User;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProfilePage extends Page {
    @FindBy (xpath = "//button[@id='goToJoin']")
    WebElement buttonGoJoin;

    @FindBy (xpath = "//div[@id='mat-tab-label-0-0']")
    WebElement loginLink;

    @FindBy (xpath = "//mat-tab-body[@id='mat-tab-content-0-0']/div/section/div[3]/span")
    WebElement emailLink;

    @FindBy (xpath = "//input[@id='mat-input-0']")
    WebElement email;

    @FindBy (xpath = "//input[@id='mat-input-1']")
    WebElement password;

    @FindBy (xpath = "//button[@class='jj-button mat-raised-button mat-primary']")
    WebElement loginButton;

    @FindBy (xpath = "(//input[@name='file'])[2]")
    WebElement uploadProfilePic;

    @FindBy(xpath = "//button[@ng-click='$ctrl.save()']")
    WebElement savePicButton;

    @FindBy (xpath = "//input[@name='file']")
    WebElement uploadProfileBackgroundPic;

    @FindBy (xpath = "//textarea[@id='commentInput']")
    WebElement commentField;

    @FindBy (css = ".layout-align-end-stretch > .md-primary")
    WebElement commentButtonSend;

    @FindBy (css = "#\\35 c7e59fa0ef52e4c5018ddf6 .reply")
    WebElement replyButton;

    @FindBy (xpath = "//button[contains(.,'delete')]")
    WebElement deleteButton;

    @FindBy (css = ".close-button")
    WebElement noDeleteComment;

    @FindBy (css = ".button-delete")
    WebElement yesDeleteComment;


    public ProfilePage login(User user) throws InterruptedException {

        new WebDriverWait(driver,20);

        Thread.sleep(1000);
        buttonGoJoin.click();

        Thread.sleep(1000);
        loginLink.click();

        Thread.sleep(1000);
        emailLink.click();

        Thread.sleep(1000);
        email.click();
        email.sendKeys(user.getLogin());

        Thread.sleep(1000);
        password.click();
        password.sendKeys(user.getPassword());

        loginButton.click();

        return new ProfilePage();
    }

    public void comment() throws InterruptedException {
        Thread.sleep(3000);
        commentField.click();
        Thread.sleep(2000);
        commentField.sendKeys("Hello, friend");

        Thread.sleep(2000);
        commentButtonSend.click();

        Thread.sleep(2000);
        replyButton.click();
        commentField.sendKeys("Hi, Alice!");
        Thread.sleep(2000);
        commentButtonSend.click();

        Thread.sleep(3000);
        deleteButton.click();
        Thread.sleep(2000);
        noDeleteComment.click();
        Thread.sleep(2000);
        deleteButton.click();
        Thread.sleep(2000);
        yesDeleteComment.click();
    }

    public void setChangePhoto() throws InterruptedException {
        Thread.sleep(6000);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        WebElement uploadElement = driver.findElement((By.cssSelector("label:nth-child(10)")));
        Thread.sleep(6000);
        executor.executeScript("arguments[0].style.visibility='visible';" + "arguments[0].style.position='relative';" + "arguments[0].style.height='500px';" + "arguments[0].style.width='500px';" + "arguments[0].style.margin='20px';" + "arguments[0].style.padding='20px';", uploadElement);
        executor .executeScript("arguments[0].scrollIntoView(true);", uploadElement);
        Thread.sleep(6000);
        uploadProfilePic.sendKeys("C:\\Users\\Walkie\\IdeaProjects\\jj_automated_tests\\src\\test\\resources\\1369460621_panda-26.jpg");
        Thread.sleep(6000);
        savePicButton.click();
    }

    public void setChangeBackgroundPhoto() throws InterruptedException {
        Thread.sleep(6000);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        WebElement uploadElement = driver.findElement((By.cssSelector("label:nth-child(9)")));
        Thread.sleep(6000);
        executor.executeScript("arguments[0].style.visibility='visible';" + "arguments[0].style.position='relative';" + "arguments[0].style.height='500px';" + "arguments[0].style.width='500px';" + "arguments[0].style.margin='20px';" + "arguments[0].style.padding='20px';", uploadElement);
        executor .executeScript("arguments[0].scrollIntoView(true);", uploadElement);
        Thread.sleep(6000);
        uploadProfileBackgroundPic.sendKeys("C:\\Users\\Walkie\\IdeaProjects\\jj_automated_tests\\src\\test\\resources\\IMG_287118.gif");
        Thread.sleep(6000);
        savePicButton.click();
    }


}

